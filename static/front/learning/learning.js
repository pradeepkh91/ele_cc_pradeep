angular.module('learning', ['ui.bootstrap', 'ui.router', 'ngAnimate']);

angular.module('learning').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'learning/partial/first/first.html',
        controller: 'FirstCtrl'
    });
});
