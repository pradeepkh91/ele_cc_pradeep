from rest_framework import serializers

from .models import Backup


class BackupFileserializer(serializers.ModelSerializer):
    class Meta:
        model = Backup
        fields = ['backup_file']