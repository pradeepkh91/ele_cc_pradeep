from django.conf.urls import url

from .views import filepicupload, addmobile, getfile

urlpatterns = [
    url(r'^addmobile', addmobile),
    url(r'^uploadfile', filepicupload),
    url(r'^getfile', getfile),

]