# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-04-11 07:32
from __future__ import unicode_literals

import backup.models
import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backup', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='backup',
            name='mobile',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='backup',
            name='backup_file',
            field=models.FileField(default=1, storage=django.core.files.storage.FileSystemStorage(location=b'/home/pradeep/Desktop/Ele_CC_PRADEEP/media_cdn'), upload_to=backup.models.upload_location, verbose_name='backup_file'),
            preserve_default=False,
        ),
    ]
