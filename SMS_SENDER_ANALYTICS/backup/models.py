from __future__ import unicode_literals

from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
from SMS_SENDER_ANALYTICS import settings


def upload_location(instance, filename):
    return "%s/%s" % (instance.mobile, filename)


class Backup(models.Model):
    mobile = models.CharField(max_length=10)
    backup_file = models.FileField('backup_file', upload_to=upload_location,
                                     storage=FileSystemStorage(location=settings.MEDIA_ROOT))