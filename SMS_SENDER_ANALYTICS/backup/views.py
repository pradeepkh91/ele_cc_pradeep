from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Backup
from .serializers import BackupFileserializer


@api_view(['POST'])
def addmobile(request):
    if request.data['mobile_number'] not in ['undefined', '', 'null']:
        var = Backup()
        var.mobile = request.data['mobile_number']
        var.save()
        data = {"message": "Mobile Saved successfully"}
    else:
        data = {"message": "Enter valid Number"}
    return Response(data)


@api_view(['POST'])
def filepicupload(request):
    profile = Backup.objects.filter(mobile=request.data['mobil'])
    if 'file' in request.data:
        if str(request.data['file']).endswith('.json'):
            name = request.data['name']
            request.data[name] = request.data['file']
            serializer = BackupFileserializer(profile[0], request.data)
            if serializer.is_valid(Exception):
                serializer.save()
                data = {'message': 'Successfully updated'}
        else:
            data = {'message': 'file type not allowed'}
        # else:
        #     pro = Backup.objects.filter(mobile=request.data['mobil']).values_list('backup_file',flat=True)
        #     pro1=map(str,pro)
        #     [Backup.objects.filter(mobile=request.data['mobil'],backup_file=x).delete()  for x in pro1  if x=='']
        #     data = {'message': 'file type not allowed'}
    else:
        data = {'message': 'please send the file'}
    return Response(data, status=status.HTTP_200_OK)


import re
import json
@api_view(['POST'])
def getfile(request):
    mobile = request.data
    print request.data
    details = Backup.objects.filter(mobile=mobile)
    list2=[]
    for i in details:
        data = json.load(open(i.backup_file.path))
        for j in data['smses']['sms']:
            if re.match(r'[a-z A-Z]{2}-[a-z A-Z]{6}', j['@address']):
                d={}
                d['address']=j['@address']
                d['date'] = j['@readable_date']
                list2.append(d)
    print list2
    return Response(list2)



